import { useRouter } from 'vue-router'
import register from '../store/register';
import store from '../store'

export function authMiddleware(to, from, next) {
    /*const token = register.getters.getToken;*/
    const token = store.getters['registerModule/getToken'];
    console.log("token", token)
    const router = useRouter();
    if (!token && to.path !== '/') {
        next('/');
    } else {
        next();
    }
}