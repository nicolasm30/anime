import { createApp } from 'vue';
import './style.css';
import App from './App.vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import router from './router';
import store from './store';

router.beforeEach((to, from, next) => {
    store.dispatch('loginModule/checkAuth')
    next()
})

const app = createApp(App)
app.use(router,VueAxios, axios)
app.use(store)
app.mount('#app')

