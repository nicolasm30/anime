import { createWebHistory, createRouter } from "vue-router";
import { authMiddleware } from "../middleware";
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import Backoffice from '../views/Backoffice.vue';
import ListofCategories from '../views/ListofCategories.vue';
import UpdateCategory from '../views/UpdateCategory.vue';
import CreateCategory from '../views/CreateCategory.vue';

const routes = [{
        path: '/',
        name: 'Home',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/backoffice',
        name: 'Backoffice',
        component: Backoffice,
        meta: { requiresAuth: true }
    },
    {
        path: '/categories',
        name: 'Categories',
        component: ListofCategories,
        meta: { requiresAuth: true }
    },
    {
        path: '/categories/edit/:name',
        name: 'Edit',
        component: UpdateCategory,
        meta: { requiresAuth: true }
    },
    {
        path: '/categories/new',
        name: 'Create',
        component: CreateCategory,
        meta: { requiresAuth: true }
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token')
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth)

    if (requiresAuth && !token) {
        next('/') // Redirigir a la página de inicio de sesión si no existe el token
    } else {
        next() // Permitir el acceso a la ruta si existe el token
    }
})

export default router;