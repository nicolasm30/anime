import axios from 'axios';
import Swal from 'sweetalert2'

const registerModule = {
    namespaced: true,
    state() {
        return {
            response: null,
            token: null
        }
    },
    actions: {
        async sendDataForm({commit}, data) {
            let message = null;
            await axios.post(`${import.meta.env.VITE_API_URL}/register`, data)
                .then(response => {
                    if (response.status === 201) {
                        Swal.fire({
                            title: 'Éxito!',
                            text: 'Te haz registrado con exito',
                            icon: 'success',
                            timer : 2000
                        })
                        commit('setToken', response.data.token)
                    }
                })
                .catch(e => {
                    message = e.response.data.message
                    Swal.fire({
                        title: 'Error!',
                        text: (message) ? `${message}` : 'Hubo un error al procesar la solicitud.',
                        icon: 'error',
                        timer : 2000
                    })
                })

        }
    },
    mutations: {
        setToken(state, token) {
            state.token = token
            localStorage.setItem('token', JSON.stringify(token))
        }
    },
    getters: {
        getToken: () => localStorage.getItem('token')
    }

}

export default registerModule