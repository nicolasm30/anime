import axios from 'axios';
import CryptoJS  from 'crypto-js';

const signatureModule =  {
    namespaced: true,
    state() {
        return{
            signature : null,
            signatureHash : null
        }
    },
    actions: {
        async fetchSignature({commit}) {
            try {
                const response = await axios.get(`${import.meta.env.VITE_API_URL}/timezone`)
                const signature = await response.data
                const { timezone } = await signature;
                commit('setHashSignature', timezone)
                return timezone
            } catch (error) {
                console.log(error)
            }

        },
    },
    mutations: {
        setSignature(state, payload) {
            state.signature = payload
        },
        setHashSignature(state, signature) {
            let temp = `${import.meta.env.VITE_PRIVATE_KEY},${import.meta.env.VITE_PUBLIC_KEY},${signature}`
            let hashSignature =  CryptoJS.SHA256(temp).toString();
            state.signatureHash = hashSignature
        }
    },
    getters: {
        getSignature(state) {
            return state.signature
        },
        getHashSignature(state) {
            return state.signatureHash
        }
    }

}

export default signatureModule
