import axios from 'axios';
import Swal from 'sweetalert2'

const loginModule = {
    namespaced: true,
    state() {
        return {
            token: localStorage.getItem('token') || null,
            expiresAt: localStorage.getItem('expiresAt') || null
        }
    },
    actions:{
        async login({commit}, data){
            try {
                const response = await axios.post(`${import.meta.env.VITE_API_URL}/login`, data)
                const token = response.data.access_token
                const expiresIn = response.data.expires_in
                const expiresAt = new Date().getTime() + expiresIn * 1000
                localStorage.setItem('token', token)
                localStorage.setItem('expiresAt', { token, expiresAt })
                commit('setToken', token)
                return true
                
            } catch (e) {
                console.log(e)
                Swal.fire({
                    title: 'Error!',
                    text: 'Hubo un error al procesar la solicitud.',
                    icon: 'error'
                })
                return false
            }
        },
        checkAuth({ state, commit }) {
            const expiresAt = state.expiresAt
            const token = state.token
            if (token && expiresAt) {
              const now = new Date().getTime()
              if (now > parseInt(expiresAt)) {
                localStorage.removeItem('token')
                localStorage.removeItem('expiresAt')
                commit('clearToken')
                router.push('/')
              }
            }
        }
    },
    mutations: {
        setToken(state, { token, expiresAt }) {
            state.token = token
            state.expiresAt = expiresAt
        },
        clearToken(state) {
          state.token = null
          state.expiresAt = null
        }
    },
    getters: {
        getToken: () => localStorage.getItem('token')
    }
}

export default loginModule