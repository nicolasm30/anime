import { createStore } from 'vuex'
import signatureModule from './store/signature'
import registerModule from './store/register'
import loginModule from './store/login'

const store = createStore({
  modules: {
    signatureModule,
    registerModule,
    loginModule
  },
})
export default store