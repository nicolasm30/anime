/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './public/**/*.html',
    './src/**/*.{js,jsx,ts,tsx,vue}',
  ],
  theme: {
    extend: {
      fontFamily:{
        'poppins': ['Poppins', 'sans-serif'],
        'roboto': ['Roboto', 'serif'],
        'archivo': ['Archivo', 'sans-serif'],
      },
      colors: {
        'purple' : '#CA38ED',
        'dark-purple' : '#3E1149',
        'light-purple': '#E487FB',
        'redwine': '#AD3C40',
        'gray' : '#C4C4C4',
        'grayLight' : '#D8D5D9'
      }
    },
  },
  plugins: [],
}

